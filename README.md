# satie4blender

This is an addon for the Blender editor, it allows interactions with [SATIE](https://gitlab.com/sat-metalab/SATIE) audio spatialization engine.

# Dependencies

These instructions are written for Ubuntu 20.04 (Focal Fossa).

This addon was developed and tested with Blender 2.9x. We generally use the [PPA from Thomas Schiex](https://launchpad.net/~thomas-schiex/+archive/ubuntu/)
```
sudo add-apt-repository ppa:thomas-schiex/blender
sudo apt update
sudo apt install blender
```
Blender 2.9x uses Python 3.8.
```
sudo apt install python3.8
```

We will also need to add some Python modules:

```
python3.8 -m pip install cython
python3.8 -m pip install pyliblo

```

# Installation

Run ./zipit.sh from this directory, it will produce satie.zip archive.
Run blender, open preferences, locate Add-ons tab, click "Install from File" button and locate the above archive. The plugin will appear as SATIE OSC in the "User" category. Activate it by clicking on the check-box.

Alternatively, run ./install.sh from this directory and it will install the plugin into the addons directory of the default Blender installation on your system. It will need to be activated in Preferences->Add-ons.

# In place installation (for developer using blender 2.92.0)

A symlink will allow to make modifications to the addon and reload in Blender without the need of reinstalling with the previous methods.

```
mkdir -p ${HOME}/.config/blender/2.92/scripts/addons/satie
# from this directory
ln -s $(pwd)/satie ${HOME}/.config/blender/2.92/scripts/addons/
```

# Quickstart


You will need to have installed [SATIE](https://gitlab.com/sat-metalab/SATIE) and make make sure it runs.
You can use this simple SATIE configuration to get started (to be executed in the SuperCollider IDE):

```
~satie = Satie(SatieConfiguration()).boot
```
Optionnaly you may want to show some windows to have some visual feedback:
```
s.meter
s.plotTree
s.makeGui
```

The plugin will create a new tab in the Tools Panel (the Tools Panel can be toggled with `n` key in a 3D viewport).

![SATIE in Tool shelf](./img/SATIE-toolshelf.png "SATIE in Tool shelf")

Click on _SATIE Tool_ tab to expand it, if necessary.

The _Configure_ button takes you to the (Preferences Panel)[#preferences-panel]


The _Activate_ button will change its label to _Deactivate_ depending on the state of the addon.
It either is ready to transmit/receive OSC messages or not. It is not an indication that it is successfully communicating with SATIE.

The plugin also adds a tab in the object properties panel:

![SATIE Object properties](./img/SATIE-Object-properties.png "SATIE Object properties screenshot")


SATIE uses plugins (here represented by the dropdown menu _Plugin Type_) in order to make available named sound generators, or sources (here represented by by the _Sound Source_ dropdown menu).
In the Blender world, any 3D object can become associated with a SATIE plugin.
Once the plugin activated, the sound parameters become part of the object's properties.
Spatialization parameters (azimuth, elevation and gain) are derived from the object's location and computed according to the listener's position, orientation and distance.
Other parameters, that are specific to the sound source in use, are displayed in the SATIE tab of the object properties and can be manipulated manually or animated.

A typical workflow is as follows:

- Choose the desired plugin
- Choose its representation. One plugin could have several versions to choose from, they will be listed in the _Sound Source_ menu.
- Clicking _Create Synth_ button will make create an instance that can now be controlled.
- The sound parameters will be displayed in the properties panel:

![Active sound properties](img/SATIE-active-properties.png "Active sound properties")

This workflow is shown in this short screencast below:

![Workflow animation](./img/SATIE_example_workflow.gif "workflow animation")


## Preferences Panel


![Addon Preferences](./img/SATIE-addon-preferences.png "Addon preferences")

- `OSC Destination` and `OSC Destination Port` control where the OSC messages will be sent. SATIE uses port 18032 by default so assuming that SATIE and Blender are running on the same computer, the default values should be good to go.

- `OSC Server` is the server that this addon is using. The `OSC Server Port` could be anything. SATIE will send some information to the addon on this port.

# Disclaimer

This is a prototype/work in progress that shows some functionality.
You can attach a SATIE instance to a blender object, move it around and hear the movement in the speakers.
There is not much error checking, no synth object management and there are bugs.
Use it at your own risk.
It has been tested on Linux only.
Merge Requests are welcome.

# Known problems

Upon re-opening a saved file,
