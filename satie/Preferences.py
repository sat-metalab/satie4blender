import logging

import bpy
from bpy.types import AddonPreferences
from typing import List, Tuple, TYPE_CHECKING

logger = logging.getLogger(__name__)

LOGGING_LEVELS = ["NOTSET", "CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"]

logging_config = None

def set_active(self, context: bpy.context) -> None:
    """
    Helper method to set the active flag on the Satie server singleton and either connect or disconnect from SATIE.

    :param context: bpy.context.object
    :return:
    """
    bpy.satie.set_connect(self.active)
    logger.info("Requested a connection with SATIE")


def set_debug(self, context: bpy.context) -> None:
    """
    Helper method to set the debug flag on the Satie singleton

    :param context:
    :return:
    """
    bpy.satie.set_debug(self.debug)
    logger.info("Set SATIE's debug mode to {self.debug}")


def provide_logging_level_enum_property(self, context: bpy.context) -> List[Tuple]:
    return [(n, n, n) for n in LOGGING_LEVELS]


def set_logging_level(self, context: bpy.context) -> None:
    prefs = context.preferences.addons['satie'].preferences


class SatieOSCPreferences(AddonPreferences):
    """
    Blender Addon Preferences
    """

    bl_idname = __package__

    active : bpy.props.BoolProperty(
        name="Active",
        description="Activate SATIE communication and connect automatically to the server",
        default=False,
        update=set_active
    )

    osc_destination : bpy.props.StringProperty(
        name="OSC Destination",
        description="Hostname or IP address of the SATIE server",
        default="localhost"
    )

    osc_destination_port : bpy.props.IntProperty(
        name="OSC Destination Port",
        description="Port of the SATIE server",
        default=18032
    )

    osc_server_port : bpy.props.IntProperty(
        name="OSC Server Port",
        description="Local OSC server port",
        default=6666
    )

    satie_debug : bpy.props.BoolProperty(
        name="Satie Debugging",
        description="Print SATIE client debug messages",
        update=set_debug
    )

    listener : bpy.props.StringProperty(
        name="Listener Object",
        description="Name of the Blender object acting as virtual microphone in the scene",
        default="Camera"
    )

    addon_logging : bpy.props.EnumProperty(
        name="Satie4Blender logging",
        description="Satie4Blender logging level",
        items=provide_logging_level_enum_property,
        update=set_logging_level
    )

    pysatie_satie_logging : bpy.props.EnumProperty(
        name="pysatie.satie logging",
        description="pysatie.satie logging level",
        items=provide_logging_level_enum_property,
        update=set_logging_level
    )

    pysatie_osc_logging : bpy.props.EnumProperty(
        name="pysatie.oscserver logging",
        description="pysatie OSC server/client logging level",
        items=provide_logging_level_enum_property,
        update=set_logging_level
    )


    def draw(self, context: bpy.context) -> None:
        col = self.layout.column(align=True)
        col.label(text="SATIE Connection")
        col.prop(self, "osc_destination")
        col.prop(self, "osc_destination_port")
        col.label(text="OSC Server")
        col.prop(self, "osc_server_port")
        col.prop(self, "listener")
        col.separator()
        col.prop(self, "active")
        col.prop(self, "satie_debug")
        col.separator()
        col.label(text="Module debugging levels")
        col.prop(self, "addon_logging")
        col.prop(self, "pysatie_satie_logging")
        col.prop(self, "pysatie_osc_logging")
