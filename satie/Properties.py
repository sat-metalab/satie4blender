import bpy
from bpy.types import Object, PropertyGroup
from bpy.props import BoolProperty, EnumProperty, StringProperty, PointerProperty
from typing import Callable, Dict, Optional


def get_plugin_family_items(self, context):
    """
    Helper method to get the list of plugin types

    :param self:
    :param context:
    :return:
    """
    satie = bpy.satie
    return [(plugin.type, plugin.type, plugin.description) for plugin in satie.plugins]


def get_synth_items(self, context):
    """
    Helper method to get the list of plugins for the selected type

    :param self:
    :param context:
    :return:
    """
    satie = bpy.satie
    return [(plugin.name, plugin.name, plugin.description) for plugin in satie.get_plugins_by_type(self.plugin_family)]


def sync_plugin_family(self, context):
    """
    Just sync the ui property (enum) with the saved one (string)
    :param self:
    :param context:
    :return:
    """
    if self.plugin_family != self.ui_plugin_family:
        self.plugin_family = self.ui_plugin_family


def set_plugin_family(self, context):
    """
    Just sync the ui property (enum) with the saved one (string)

    :param self:
    :param context:
    :return:
    """
    if self.ui_plugin_family != self.plugin_family:
        self.ui_plugin_family = self.plugin_family


def sync_synth(self, context):
    """
    Just sync the ui property (enum) with the saved one (string)
    :param self:
    :param context:
    :return:
    """
    if self.synth != self.ui_synth:
        self.synth = self.ui_synth


def set_synth(self, context):
    """
    Just sync the ui property (enum) with the saved one (string)
    :param self:
    :param context:
    :return:
    """
    if self.ui_synth != self.synth:
        self.ui_synth = self.synth


def set_active(self, context):
    """
    Helper method to set the active flag remotely via OSC
    :param self:
    :param context:
    :return:
    """
    satie = bpy.satie
    state: int = int(self.active)
    satie.node_state(context.object.name, state)


def get_3d_objects(self, context):
    return [(obj.name, obj.name, obj.name) for obj in bpy.data.objects]


def sync_listener(self, context):
    if self.listener != self.ui_listener:
        self.listener = self.ui_listener


def get_listener(self, context):
    if self.listener != self.ui_listener:
        self.listener = self.ui_listener


def dynamic_property(func: Dict) -> Callable:
    """
    Function generator that .
    """
    def synth_property(*args, **kwargs) -> PointerProperty:
        """

        :param args: first argument must be a dictionary with Blender Property: {'property_name': bpy.props.FloatProperty(default=0.1)}
        :param kwargs: specific keywords needed by enclosed methods
        :return: Pointer to new property
        """
        Prop: PropertyGroup = type(
            # type name
            str("SynthParameters"),
            # base class
            (bpy.types.PropertyGroup,),
            # dictionary of properties
            # func(*args)
            {
                '__annotations__' : func(*args)
            }
        )
        bpy.utils.register_class(Prop)
        PropPointer: PointerProperty = bpy.props.PointerProperty(name=func(kwargs.get('param_type')), type=Prop)
        setattr(func(kwargs['blender_type']), func(kwargs['param_type']), PropPointer)
        return PropPointer

    return synth_property


class SatieSynthProperty(object):
    """
    Blender Object properties for Satie synth instance, used as a 'struct' for property mapping
    """

    def __init__(self, **kwargs) -> None:
        self.name: Optional[str] = kwargs.setdefault("name", None)
        self.property_dict: Dict = {}
        self.blender_type: Object = Object  # we want to attach those to the object
        self.param_type: str = "SynthParameters"
        self._prop: PointerProperty = None

    def generate_properties(self) -> Dict:
        prop_ptr: Dict = self.create_synth_property(self.property_dict, blender_type=self.blender_type, param_type=self.param_type)
        self._prop = prop_ptr
        return self._prop

    @staticmethod
    @dynamic_property
    def create_synth_property(prop_dict, **kwargs):
        return prop_dict


class SatieObjectProperties(PropertyGroup):
    """
    Static properties, common to all SATIE objects
    """

    def set_debug(self, context):
        """
        Helper method to set the debug flag on the synth instance
        :param self:
        :param context:
        :return:
        """
        satie = bpy.satie
        debug: int = 1 if self.debug is True else 0
        satie.set_debug(debug)

    enabled: BoolProperty(
        name="Enabled",
        description="Enable/disable the synth, an enabled synth is instantiated in the SATIE server",
        default=False
    )

    group: StringProperty(
        name="Group",
        description="Instrument/FX Group",
        default="default"
    )

    ui_plugin_family: EnumProperty(
        name="Plugin Type",
        description="SATIE Plugin Type",
        items=get_plugin_family_items,
        update=sync_plugin_family
    )

    plugin_family: StringProperty(
        name="Plugin Type (String)",
        description="SATIE Plugin Type",
        update=set_plugin_family
    )

    ui_listener: EnumProperty(
        name="Listener candidates",
        description="List of objects qualifying as listener",
        items=get_3d_objects,
        update=sync_listener
    )

    listener: StringProperty(
        name="Listener",
        description="Listener object",
        update=get_listener
    )

    ui_synth: EnumProperty(
        name="Sound Source",
        description="SATIE Plugin to Use",
        items=get_synth_items,
        update=sync_synth
    )

    synth: StringProperty(
        name="Sound Source (String)",
        description="SATIE Plugin to Use",
        update=set_synth
    )

    debug: BoolProperty(
        name="Debug",
        description="Debug this object's synth in the console",
        default=False,
        update=set_debug
    )

    active: BoolProperty(
        name="Active",
        description="Synth state (pause/play)",
        default=False,
        update=set_active
    )
